package dk.n3xuz.hookregistry.context;

import dk.n3xuz.hookregistry.exceptions.ContextLockedException;

public abstract class Context {
    protected boolean locked;

    protected Context() {
    }

    public void lock() {
        locked = true;
    }

    public void unlock() {
        locked = false;
    }

    protected void checkLock() {
        if (locked)
            throw new ContextLockedException();
    }
}
