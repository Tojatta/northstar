package dk.n3xuz.hookregistry.filters;

import dk.n3xuz.hookregistry.Hook;
import dk.n3xuz.hookregistry.context.Context;

import java.util.function.BiPredicate;

public interface Filter<T extends Context> extends BiPredicate<Hook<T>, T> {

    @Override
    boolean test(Hook<T> hook, T context);
}
