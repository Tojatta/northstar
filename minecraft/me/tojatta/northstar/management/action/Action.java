package me.tojatta.northstar.management.action;

/**
 * Created by Tojatta on 3/1/2016.
 */
public enum Action {
    /**
     * Types of actions.
     */
    RIGHT_CLICK, LEFT_CLICK, MIDDLE_CLICK, KEY_PRESS
}
