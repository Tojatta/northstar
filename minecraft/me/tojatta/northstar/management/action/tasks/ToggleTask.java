package me.tojatta.northstar.management.action.tasks;

import me.tojatta.northstar.management.action.ActionTask;
import me.tojatta.northstar.management.module.ToggleableModule;

/**
 * Created by Tojatta on 3/1/2016.
 */
public class ToggleTask extends ActionTask {

    /**
     * ToggleableModule
     */
    private ToggleableModule module;

    /**
     * ToggleTask constructor.
     * @param module
     */
    public ToggleTask(ToggleableModule module) {
        this.module = module;
    }

    @Override
    public void invoke() {
        module.toggle();
    }

    /**
     * Returns toggleablemodule
     * @return module.
     */
    public ToggleableModule getModule() {
        return module;
    }
}
