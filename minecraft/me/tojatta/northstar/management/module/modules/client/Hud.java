package me.tojatta.northstar.management.module.modules.client;

import dk.n3xuz.hookregistry.Hook;
import me.tojatta.northstar.Northstar;
import me.tojatta.northstar.hooks.RenderContext;
import me.tojatta.northstar.management.module.ToggleableModule;
import me.tojatta.northstar.management.module.annotations.ModuleManifest;
import net.minecraft.client.renderer.GlStateManager;

/**
 * Created by Tojatta on 3/1/2016.
 */
@ModuleManifest(name = "HUD", aliases = {"hud", "overlay"}, description = "Draws the hack overlay", author = "Tojatta")
public class Hud extends ToggleableModule {

    private final Hook<RenderContext.Screen> screenHook;

    public Hud() {
        screenHook = new Hook<RenderContext.Screen>("screen_hook") {
            @Override
            public void call(RenderContext.Screen context) {
                drawWatermark(context);
            }
        };
        setState(true);
    }

    private void drawWatermark(RenderContext.Screen context){
        GlStateManager.pushMatrix();
        GlStateManager.disableBlend();
        mc.fontRendererObj.drawStringWithShadow(Northstar.HACKNAME, context.getResolution().getScaledWidth() - mc.fontRendererObj.getStringWidth(Northstar.HACKNAME) - 2, 2, 0xFFFFFFFF);
        GlStateManager.enableBlend();
        GlStateManager.popMatrix();
    }

    @Override
    public void onEnable() {
        super.onEnable();
        Northstar.getInstance().getHookRegistry().register(screenHook);
    }

    @Override
    public void onDisable() {
        super.onDisable();
        Northstar.getInstance().getHookRegistry().unregister(screenHook);
    }
}
