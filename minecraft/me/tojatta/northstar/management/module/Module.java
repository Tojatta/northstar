package me.tojatta.northstar.management.module;

import me.tojatta.northstar.interfaces.Labelable;
import net.minecraft.client.Minecraft;

/**
 * Created by Tojatta on 2/26/2016.
 */
public class Module implements Labelable {

    /**
     * Module name.
     */
    protected String name;

    /**
     * Description for the module.
     */
    protected String description;

    /**
     * Author of the module.
     */
    protected String author;

    /**
     * Minecraft.
     */
    protected Minecraft mc;

    /**
     * Module constructor.
     */
    public Module() {
        mc = Minecraft.getMinecraft();
    }

    /**
     * Returns module name.
     *
     * @return name
     */
    @Override
    public String getLabel() {
        return name;
    }

    /**
     * Sets module name.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns module description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets module description.
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns module author.
     *
     * @return author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets module author.
     *
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }


}
