package me.tojatta.northstar.management.module.interfaces;

/**
 * Created by Tojatta on 2/26/2016.
 */
public interface Toggleable {

    /**
     * Running state.
     *
     * @return isRunning
     */
    boolean isRunning();

    /**
     * When toggled on.
     */
    void onEnable();

    /**
     * When toggled off.
     */
    void onDisable();

    /**
     * When toggled.
     */
    void onToggle();

    /**
     * Toggle.
     */
    void toggle();

}
