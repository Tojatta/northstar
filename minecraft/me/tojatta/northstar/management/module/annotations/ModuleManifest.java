package me.tojatta.northstar.management.module.annotations;

import me.tojatta.northstar.management.module.ModuleCategory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Tojatta on 2/26/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ModuleManifest {

    /**
     * Module name.
     *
     * @return name
     */
    String name();

    /**
     * Module aliases.
     *
     * @return aliases
     */
    String[] aliases();

    /**
     * Module description.
     *
     * @return description
     */
    String description();

    /**
     * Module key.
     * Defaults to 'NONE' key.
     *
     * @return key
     */
    String key() default "NONE";

    /**
     * Module author.
     *
     * @return author
     */
    String author();

    /**
     * Module category.
     * Defaults to a client module.
     *
     * @return category
     */
    ModuleCategory category() default ModuleCategory.CLIENT;
}
