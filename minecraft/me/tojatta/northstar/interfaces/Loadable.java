package me.tojatta.northstar.interfaces;

/**
 * Created by Tojatta on 2/24/2016.
 */
public interface Loadable {

    /**
     * Loads
     */
    boolean load();

    /**
     * Saves
     */
    boolean save();
}
