package me.tojatta.northstar.interfaces;

/**
 * Created by Tojatta on 2/24/2016.
 */
public interface Labelable {

    /**
     * String Label
     * @return
     */
    String getLabel();

}
