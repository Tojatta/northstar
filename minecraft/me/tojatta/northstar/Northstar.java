package me.tojatta.northstar;

import dk.n3xuz.hookregistry.HookRegistry;
import dk.n3xuz.hookregistry.impl.BasicHookRegistry;
import me.tojatta.northstar.interfaces.Loadable;
import me.tojatta.northstar.management.action.ActionManager;
import me.tojatta.northstar.management.module.ModuleManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tojatta on 2/16/2016.
 */
public class Northstar implements Loadable {

    public static final String HACKNAME = "Northstar";
    public static final String BUILD = "160302";
    public static final String[] DEVS = {"Tojatta"};

    private static volatile Northstar instance;

    private static List<Loadable> loadables;

    private HookRegistry hookRegistry;

    /**
     * Managers
     */
    private ModuleManager moduleManager;
    private ActionManager actionManager;

    public Northstar() {
        loadables = new ArrayList<>();
    }

    /**
     * Adds Loadable object to loadables list and loads it.
     *
     * @param loadable
     */
    private boolean registerLoadables(Loadable... loadable) {
        Collections.addAll(loadables, loadable);
        loadables.stream().forEach(l -> l.load());
        return true;
    }

    /**
     * Returns Northstar instance.
     *
     * @return northstar
     */
    public static Northstar getInstance() {
        if (instance == null) {
            instance = new Northstar();
        }
        return instance;
    }

    /**
     * Returns ModuleManager and sets it if it's null.
     *
     * @return moduleManager
     */
    public ModuleManager getModuleManager() {
        if (moduleManager == null) {
            moduleManager = new ModuleManager();
        }
        return moduleManager;
    }

    /**
     * Returns ActionManager and sets it if it's null.
     *
     * @return ActionManager
     */
    public ActionManager getActionManager() {
        if (actionManager == null) {
            actionManager = new ActionManager();
        }
        return actionManager;
    }

    /**
     * Returns HookRegistry and sets it if it's null.
     *
     * @return hookRegistry
     */
    public HookRegistry getHookRegistry() {
        if (hookRegistry == null) {
            hookRegistry = new BasicHookRegistry();
        }
        return hookRegistry;
    }

    @Override
    public boolean load() {
        return registerLoadables(getModuleManager(), getActionManager());
    }

    @Override
    public boolean save() {
        return false;
    }
}
